#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <array>"
    exit    
fi

array=("$@")

printf "Enter element to search: "
read num

ctr=0
for i in ${array[@]}; do
    if [ $i -eq $num ]; then
        echo "Element $num found at index $ctr."
        exit
    fi
    ctr=$(($ctr + 1))
done

echo "$num not found."
