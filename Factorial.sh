#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <number>"
    exit    
fi

num=1
num=$1
fact=1
for i in $(seq $num -1 1); do
    fact=$((fact * i))
done

echo $fact
    
