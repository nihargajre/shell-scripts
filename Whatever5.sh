#!/bin/bash

if [ $# -lt 2 ]; then
    echo "Usage: $0 <number 1> <number 2>"
    exit    
fi

echo "Sum: $(($1 + $2))"
