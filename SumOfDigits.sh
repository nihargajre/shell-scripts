#!/bin/sh

if [ $# -lt 1 ]; then
    echo "Usage: $0 <number>"
    exit    
fi

num=1
num=$1
sum=0
for i in $(echo $num | grep -o .); do
    sum=$(($sum + $i))
done

echo $sum
