#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <string>"
    exit
fi

str=$1
revstr=$(echo $str | rev)

if [ $str = $revstr ]; then
    echo "Palindrome"
else
    echo "Not Palindrome"
fi
