#!/bin/bash

if [ $# -lt 2 ]; then
    echo "Usage: $0 <file> <substring>"
    exit    
fi
file=$1
substr=$2
echo "Match Count: $(grep -o $substr $file | wc -l)"

