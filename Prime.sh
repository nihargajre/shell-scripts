#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <number>"
    exit    
fi

num=1
num=$1

num2=$(($num - 1))

for i in $(seq 2 $num2); do
    rem=$(echo "$num % $i" | bc)
    if [ $rem -eq 0 ]; then
        echo "Not prime"
        exit
    fi
done

echo "Prime"
