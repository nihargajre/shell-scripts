#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <file/directory>"
    exit    
fi

if [ -f $1 ]; then
    echo "File"
elif [ -d $1 ]; then
    echo "Directory"
fi
